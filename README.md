# Livres à lire:

- Le jeune noir à l'épée - Abd al Malik
- Lord of the ring - R. R. Tolkien
- Germinal - Émile Zola
- La Fabrique de l'Opinion publique - Noam Chomsky 
- 1Q84 - Haruki Murakami
- Le temps est assassin - Michel Bussi

# Lu
- Guerre et Paix - Léon Tolstoï
- L'étranger - Albert Camus
- La peste - Camus
- Mur Méditerranée - Louis-Philippe Dalembert
